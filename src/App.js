import Startup from "./components/startup/Startup.jsx";
import Profile from "./components/profile/Profile.jsx";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Translation from "./components/translation/Translation.jsx";

function App() {
  return (
    <div className="App">   
     
      <Router>
        <Switch> 
          <Route exact path="/" component={Startup}/>
          <Route path="/profile" component={Profile}/>
          <Route path="/translation" component={Translation} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
