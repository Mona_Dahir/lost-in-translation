
// Gets the item from localStorage with key, 
// if item is in json form return the json object else return null 
export const getJsonFormLocalStorage = (key) => {
    try {
        return JSON.parse(localStorage.getItem(key));
    } catch {
        return null;
    }
}