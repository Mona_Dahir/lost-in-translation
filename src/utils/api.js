

//const BASE_URL = "http://localhost:3001/api"
const BASE_URL = "/api"

function API() {

    // Get the user from the api or if the user dose not exists a new user is created and returned.
    this.getUser = async (name) => {
        let res = await fetch(`${BASE_URL}/users?name=${name}`)
        let user = await res.json()

        if (user.length === 0) {
            const body = { name };
            res = await fetch(`${BASE_URL}/users`, {
                method: 'post',
                body:    JSON.stringify(body),
                headers: { 'Content-Type': 'application/json' },
            })
            return await res.json()
        }
        console.log(user)
        return user[0]    
    }

    // Get the 10 latest translations for the user with a id equal to userId in the api, that is not deleted.
    this.getTranslations = async (userId) => {
        const res = await fetch(`${BASE_URL}/users/${userId}/translations?_sort=id&_order=desc&_limit=10&deleted=false`)
        return await res.json();
    }

    // Adds a new translation to the api for the user with id equal to userId and with the translation text equal to translationsText.
    this.addTranslations = async (translationsText, userId) => {
        const body = { userId, text: translationsText, deleted: false }
        const res = await fetch(`${BASE_URL}/translations`, {
            method: 'post',
            body:    JSON.stringify(body),
            headers: { 'Content-Type': 'application/json' },
        })
        return await res.json()
    }

    // Sets the translation to deleted an updates the api.
    this.clearTranslation = async (translation) => {
        translation.deleted = true;
        
        const res = await fetch(`${BASE_URL}/translations/${translation.id}`, {
            method: 'put',
            body:    JSON.stringify(translation),
            headers: { 'Content-Type': 'application/json' },
        })
        return await res.json()
    }

    // Sets all translations for the user to deleted and updates the api. 
    this.clearTranslations = async (userId) => {
        let res = await fetch(`${BASE_URL}/users/${userId}/translations?deleted=false`)
        let translations = await res.json();

        await Promise.all(
            translations.map(async translation => await this.clearTranslation(translation))
        )
        return translations
    }

}



const api = new API()
export default api