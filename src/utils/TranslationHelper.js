import imgA from '../assets/individial_signs/a.png'
import imgB from '../assets/individial_signs/b.png'
import imgC from '../assets/individial_signs/c.png'
import imgD from '../assets/individial_signs/d.png'
import imgE from '../assets/individial_signs/e.png'
import imgF from '../assets/individial_signs/f.png'
import imgG from '../assets/individial_signs/g.png'
import imgH from '../assets/individial_signs/h.png'
import imgI from '../assets/individial_signs/i.png'
import imgJ from '../assets/individial_signs/j.png'
import imgK from '../assets/individial_signs/k.png'
import imgL from '../assets/individial_signs/l.png'
import imgM from '../assets/individial_signs/m.png'
import imgN from '../assets/individial_signs/n.png'
import imgO from '../assets/individial_signs/o.png'
import imgP from '../assets/individial_signs/p.png'
import imgQ from '../assets/individial_signs/q.png'
import imgR from '../assets/individial_signs/r.png'
import imgS from '../assets/individial_signs/s.png'
import imgT from '../assets/individial_signs/t.png'
import imgU from '../assets/individial_signs/u.png'
import imgV from '../assets/individial_signs/v.png'
import imgW from '../assets/individial_signs/w.png'
import imgX from '../assets/individial_signs/x.png'
import imgY from '../assets/individial_signs/y.png'
import imgZ from '../assets/individial_signs/z.png'
 
const charToImgMap = {
    'a': imgA,
    'b': imgB,
    'c': imgC,
    'd': imgD,
    'e': imgE,
    'f': imgF,
    'g': imgG,
    'h': imgH,
    'i': imgI,
    'j': imgJ,
    'k': imgK,
    'l': imgL,
    'm': imgM,
    'n': imgN,
    'o': imgO,
    'p': imgP,
    'q': imgQ,
    'r': imgR,
    's': imgS,
    't': imgT,
    'u': imgU,
    'v': imgV,
    'w': imgW,
    'x': imgX,
    'y': imgY,
    'z': imgZ,
}


function translateCharacter(char) {
    char = char.toLowerCase()
    if (char.match(/[a-z]/g)) { 
        return charToImgMap[char]
    }
    return null
}
 
export default translateCharacter