
import { Redirect } from "react-router-dom";

// checks if user is login, if not login redirect to setup page else render Component 
function withAuth(Component){

    return function(){

        if(localStorage.getItem('token')) {
            return <Component/>
        } else {
            return <Redirect to="/"/>
        }
    } 
}
export default withAuth