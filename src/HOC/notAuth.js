import { Redirect } from "react-router-dom";

// checks if user is login, if login redirect to translation page else render Component 
function notAuth(Component){

    return function(){

        if(localStorage.getItem('token')) {
            return <Redirect to="/translation"/>
        } else {
            return <Component/>
        }
    } 
}

export default notAuth