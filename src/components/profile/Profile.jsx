import React, { useEffect, useState } from 'react'
import { useHistory } from "react-router-dom"
import withAuth from '../../HOC/WithAuth'
import api from '../../utils/api'
import TranslationBox from '../translationBox/TranslationBox.jsx'
import Header from "../header/Header.jsx"
import Styles from './styles.module.css'
import { getJsonFormLocalStorage } from '../../utils/localStorageHelper'


function Profile() {
    const [translationHistory, setTranslationHistory] = useState([]);

    const history = useHistory();


    // Clear translations
    const handleClearTranslation = () =>{
        const user = getJsonFormLocalStorage("token")
        if (!user) {
            history.push("/")
            return
        }

        api.clearTranslations(user.id)
            .then(() => { setTranslationHistory([]) })
            .catch((error) => { alert(error) })
    } 

     // logouts the user 
    const handleLogout = () =>{
        localStorage.removeItem('token');
        history.push("/")
    }

    useEffect(() => {
        const user = getJsonFormLocalStorage("token")
        if (!user) {
            history.push("/")
            return
        }

        api.getTranslations(user.id)
            .then((translations) => {setTranslationHistory(translations)})
            .catch((error) => { alert(error) })
    }, [history])

    return (
        <div>
            <Header profile/>
            <div className={[Styles.button, 'page-top'].join(' ')}>
                <h1>My profile</h1>
                <button onClick={handleClearTranslation}>Clear translations</button>
                <button onClick={handleLogout}>Logout</button>
            </div>
            <div className="page-bottom">
                <h1>Translation history</h1>
                {translationHistory.map((translationText) => {
                    return (
                        <div className={Styles.translation} key={translationText.id}>
                        <h2>{translationText.text}</h2>
                        <TranslationBox text={translationText.text}/>
                    </div>)
                } )}
            </div>
        </div>
    )
}
export default withAuth(Profile)