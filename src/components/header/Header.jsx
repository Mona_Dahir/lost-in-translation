import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import Styles from './styles.module.css'
import { useHistory } from "react-router-dom"
import Logo from '../logo/Logo';
import { getJsonFormLocalStorage } from '../../utils/localStorageHelper'


function Header() {
    const history = useHistory();

    const user = getJsonFormLocalStorage("token")

    //Goes to profile page when logged in
    const handleUserProfile = () => {
        history.push("/profile")
    }
    //Goes to translation page when logged in
    const handleHomeClick = () => {
      history.push("/translation")
    }

    return (
      <div className={Styles.header}>
        <button onClick={handleHomeClick} className={Styles['header-text']}>
          { (user) ? <Logo/> : null } 
          <h2>Lost in translation</h2>
        </button>
          
         {(user)?
            <button onClick={handleUserProfile}>
              <h4>{user.name}</h4>
              <AccountCircleIcon className={Styles['profile-icon']} style={ {fontSize:60}} />  
          </button> 
          : 
          <div/> }
      </div>
    );
  }
  
  export default Header