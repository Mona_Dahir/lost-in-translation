import React from 'react'
import Styles from './styles.module.css'
import KeyboardOutlinedIcon from '@material-ui/icons/KeyboardOutlined';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';


export default function TextInput({ placeholder, onSubmit, maxlength }) {

    // Adds input text to onsubmit prop
    const handleSubmit = (e) => {
        e.preventDefault()
        onSubmit(e.target.textbox.value)
    }

    return (
        <form className={Styles.input} onSubmit={handleSubmit}>
            <KeyboardOutlinedIcon style={{fontSize:50}}/>
            <input type="text" name="textbox" placeholder={placeholder} maxLength={maxlength} required/> 
            <button type="submit"><ArrowForwardRoundedIcon style={{fontSize:40}}/></button>
        </form>
    )
}