import React from 'react'
import { useHistory } from "react-router-dom"
import api from '../../utils/api';
import Header from "../header/Header.jsx";
import Logo from '../logo/Logo';
import TextInput  from '../TextInput/TextInput'
import Styles from './styles.module.css'
import notAuth from '../../HOC/notAuth'


function Startup() {

    //Router
    const history = useHistory();

    //Takes in name of user
    //Calls get user api and gets user if exists or adds new user name
    //Redirect to translation page
    //catch then any errors
    function handleSubmit(name) {
        api.getUser(name)
            .then((user) => {
                localStorage.setItem("token", JSON.stringify(user))
                history.push("/translation")})
            .catch((error) => { alert(error) })
    }

    return (
        <div>
            <Header/>
            <div className="page-top">
                <div className={Styles['header-div']}>
                    <Logo/>
                    <div className={Styles['text-div']}>
                        <h1>Lost in Translation</h1>
                        <h3>Get started</h3>
                    </div>    
                </div>                

                <div className={Styles['input-div']}>
                    <TextInput placeholder="What's your name?" onSubmit={handleSubmit}/>
                </div>
            </div>
        </div>
    )
}

export default notAuth(Startup)
