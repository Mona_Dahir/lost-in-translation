import React from 'react'
import logo from "../../assets/Logo.png"
import splash from "../../assets/Splash.svg"
import Styles from './styles.module.css'

export default function Logo() {
    return (
        <div className={[Styles['image-div']]}>
            <img src={splash} alt="icon" />
            <img className={Styles['logo']} src={logo} alt="icon" />                       
        </div>
    )
}
