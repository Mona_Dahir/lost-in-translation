
import React, {useState} from 'react' 
import { useHistory } from "react-router-dom"
import withAuth from '../../HOC/WithAuth'
import TranslationBox from '../translationBox/TranslationBox.jsx'
import TextInput  from '../TextInput/TextInput.jsx'
import api from '../../utils/api'
import Header from "../header/Header.jsx"
import { getJsonFormLocalStorage } from '../../utils/localStorageHelper'

function Translation() {
    //Input text state
    const [text,setText] = useState ('')
    //Router
    const history = useHistory()

    //Takes in input text
    //Gets user
    //If no user or not valid or other error redirect to start page
    //Adds text and userId to db & setText state 
    const handleOnSubmit = (text) =>{
        const user = getJsonFormLocalStorage("token")
        if (!user) {
            history.push("/")
            return
        }

        api.addTranslations(text, user.id)
            .then(() => { setText(text); })
            .catch((error) => { alert(error) })  
    }
    

    return (
        <div>
             <Header profile/>
            <div className="page-top">
                <TextInput placeholder="Translate" maxlength={40} onSubmit={handleOnSubmit}/>
            </div>
            <div className="page-bottom">
                <TranslationBox text={text}/>
            </div>
        </div>
    )
}

export default withAuth(Translation)
