import Styles from './styles.module.css'
import translateCharacter from '../../utils/TranslationHelper'

function TranslationBox({text}) {

    return (
        <div className={Styles.box}>
            <div className={Styles.translation}>
                {[...text].map((character, index) => {
                    const img = translateCharacter(character)
                    if (img) 
                    {
                        return  <div className={Styles.sign} key={index}> <img src={img} alt={ `sing ${character}` }/> </div>
                    }
                    return <div className={Styles.sign} key={index}> <p>{character}</p> </div>
                } )}
            </div>
            <div className={Styles.footer}>
                <p>Translation</p>
            </div>
        </div>
    )
}

export default TranslationBox

